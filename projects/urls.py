from django.urls import path
from projects.views import project_instance, project_details, create_project

urlpatterns = [
    path("", project_instance, name="list_projects"),
    path("<int:id>/", project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]

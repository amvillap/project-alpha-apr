from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProject

# Create your views here.


@login_required(login_url="login")
def project_instance(request):
    project_instances = Project.objects.filter(owner=request.user)
    context = {"project_instances": project_instances}
    return render(request, "projects/projects.html", context)


@login_required(login_url="login")
def project_details(request, id):
    project_detail = get_object_or_404(Project, id=id)
    context = {"project_detail": project_detail}
    return render(request, "projects/details.html", context)


@login_required(login_url="login")
def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProject()
        context = {"createproject_form": form}
        return render(request, "projects/createproject.html", context)

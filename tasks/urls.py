from django.urls import path
from tasks.views import create_task, view_mytasks

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", view_mytasks, name="show_my_tasks"),
]

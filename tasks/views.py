from django.shortcuts import render, redirect
from tasks.forms import CreateTask
from django.contrib.auth.decorators import login_required
from tasks.models import Task


# Create your views here.
@login_required(login_url="login")
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTask()
        context = {
            "createtask_form": form,
        }
        return render(request, "tasks/createtask.html", context)


@login_required(login_url="login")
def view_mytasks(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "mytasks": task,
    }
    return render(request, "tasks/mytasks.html", context)
